#include <iostream>

//recursive instantiation of variadic templates
namespace recursiveInstantiation
{
    void print()
    {}

    template <typename F, typename ... T>
    void print(F&& first, T&& ... t)
    {
        std::cout << first << " ";
        print(t...);
    }
}


//initializer list C++11 style
namespace initializerList
{
    template<typename T>
    void print(T t)
    {
        std::cout << t << " ";
    }


    template <typename ...T>
    void print(T&& ... t)
    {
        (void)std::initializer_list<int> { (print<T>(t), 0)...};
    }
}

//

template <typename ... T>
void printValues(T&&... t)
{
    recursiveInstantiation::print(std::forward<T>(t)...);
    //initializerList::print(std::forward<T>(t)...);
    std::cout << "\n";
}


int main()
{
    printValues("blue", 'g', "6", 89, 4.56789);
    getchar();
    return 0;
}